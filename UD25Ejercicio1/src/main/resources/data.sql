DROP table IF EXISTS Articulos;
DROP table IF EXISTS Fabricantes;



CREATE TABLE Fabricantes (
	id INT AUTO_INCREMENT, 
	nombre NVARCHAR(100), 
	PRIMARY KEY (id)
);

CREATE TABLE Articulos (
	id INT AUTO_INCREMENT, 
	nombre NVARCHAR(100), 
	precio INT, 
	id_Fabricante INT NOT NULL, 
	PRIMARY KEY (id), 
	FOREIGN KEY (id_Fabricante) 
	REFERENCES Fabricantes(id)
);

insert into Fabricantes(nombre) values('Corsair');
insert into Fabricantes(nombre) values('Gigabite');
insert into Fabricantes(nombre) values('Asus');
insert into Fabricantes(nombre) values('Seagate');

insert into Articulos(nombre, precio, id_Fabricante) values('Hdd Barracuda 2Tb', 79, 4);
insert into Articulos(nombre, precio, id_Fabricante) values('RTX 2070', 589, 2);
insert into Articulos(nombre, precio, id_Fabricante) values('MotherBoard Hero', 324, 3);
insert into Articulos(nombre, precio, id_Fabricante) values('cpu LiquidRef', 120, 1);

