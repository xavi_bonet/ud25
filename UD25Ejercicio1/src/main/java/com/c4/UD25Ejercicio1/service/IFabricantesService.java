package com.c4.UD25Ejercicio1.service;

import java.util.List;
import com.c4.UD25Ejercicio1.dto.Fabricantes;




public interface IFabricantesService {

	//Metodos del CRUD
	public List<Fabricantes> listarFabricantes(); //Listar All 
	
	public Fabricantes guardarFabricante(Fabricantes fabricantes);	//Guarda un Fabricante CREATE
	
	public Fabricantes fabricanteXID(Long id); //Leer datos de un Fabricante READ
	
	public Fabricantes actualizarFabricante(Fabricantes fabricantes); //Actualiza datos del Fabricante UPDATE
	
	public void eliminarFabricante(Long id);// Elimina el Fabricante DELETE
	
	
}
