package com.c4.UD25Ejercicio1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD25Ejercicio1.dao.IFabricantesDAO;
import com.c4.UD25Ejercicio1.dto.Fabricantes;



@Service
public class FabricantesServiceImpl implements IFabricantesService{
	
	@Autowired
	IFabricantesDAO iFabricantesDAO;

	@Override
	public List<Fabricantes> listarFabricantes() {
		return iFabricantesDAO.findAll();
	}

	@Override
	public Fabricantes guardarFabricante(Fabricantes fabricantes) {
		return iFabricantesDAO.save(fabricantes);
	}

	@Override
	public Fabricantes fabricanteXID(Long id) {
		// TODO Auto-generated method stub
		return iFabricantesDAO.findById(id).get();
	}

	@Override
	public Fabricantes actualizarFabricante(Fabricantes fabricantes) {
		return iFabricantesDAO.save(fabricantes);
	}

	@Override
	public void eliminarFabricante(Long id) {
		iFabricantesDAO.deleteById(id);
	}

	
	
	

}


