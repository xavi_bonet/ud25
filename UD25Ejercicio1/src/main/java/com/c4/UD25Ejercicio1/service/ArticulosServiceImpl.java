package com.c4.UD25Ejercicio1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD25Ejercicio1.dao.IArticulosDAO;
import com.c4.UD25Ejercicio1.dto.Articulos;



@Service
public class ArticulosServiceImpl implements IArticulosService{
	
	@Autowired
	IArticulosDAO iArticulosDAO;

	@Override
	public List<Articulos> listarArticulos() {
		return iArticulosDAO.findAll();
	}

	@Override
	public Articulos guardarArticulo(Articulos articulos) {
		return iArticulosDAO.save(articulos);
	}

	@Override
	public Articulos articuloXID(Long id) {
		return iArticulosDAO.findById(id).get();
	}

	@Override
	public Articulos actualizarArticulo(Articulos articulos) {
		return iArticulosDAO.save(articulos);
	}

	@Override
	public void eliminarArticulo(Long id) {
		iArticulosDAO.deleteById(id);
	}
	
	

}


