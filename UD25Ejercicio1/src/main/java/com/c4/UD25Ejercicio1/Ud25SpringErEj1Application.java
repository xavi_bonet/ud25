package com.c4.UD25Ejercicio1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud25SpringErEj1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud25SpringErEj1Application.class, args);
	}

}
