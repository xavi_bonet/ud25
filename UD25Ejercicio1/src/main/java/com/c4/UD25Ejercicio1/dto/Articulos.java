package com.c4.UD25Ejercicio1.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Articulos") // en caso que la tabala sea diferente
public class Articulos {

	// Atributos de entidad Fabricante
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // busca ultimo valor e incrementa desde id final de db
	private Long id;
	@Column(name = "nombre") 
	private String nombre;
	@Column(name = "precio") 
	private int precio;
	
	@ManyToOne
	@JoinColumn(name="idFabricante")
	private Fabricantes Fabricantes;

	// Constructores
	public Articulos() {

	}

	/**
	 * @param id
	 * @param nombre
	 * @param precio
	 * @param idFabricante
	 */
	public Articulos(Long id, String nombre, int precio, Fabricantes fabricantes) {
		// super();
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.Fabricantes = fabricantes;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the precio
	 */
	public int getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}

	/**
	 * @return the fabricantes
	 */
	public Fabricantes getFabricantes() {
		return Fabricantes;
	}

	/**
	 * @param fabricantes the fabricantes to set
	 */
	public void setFabricantes(Fabricantes fabricantes) {
		Fabricantes = fabricantes;
	}

	@Override
	public String toString() {
		return "Articulos [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", Fabricantes=" + Fabricantes
				+ "]";
	}
	
	

	
	
	
	

}
