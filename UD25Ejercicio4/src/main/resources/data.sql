DROP table IF EXISTS Salas;
DROP table IF EXISTS Peliculas;


CREATE TABLE Peliculas (
	codigo_Pelicula INT AUTO_INCREMENT, 
	nombre NVARCHAR(100), 
	calificacion_edad INT,
	PRIMARY KEY (codigo_Pelicula)
);

CREATE TABLE Salas (
	codigo_Sala INT AUTO_INCREMENT, 
	nombre NVARCHAR(100), 
	pelicula INT NOT NULL, 
	PRIMARY KEY (codigo_Sala), 
	FOREIGN KEY (pelicula) REFERENCES Peliculas(codigo_Pelicula)
);

insert into Peliculas (nombre, calificacion_edad)values('Pelicula1',3);
insert into Peliculas (nombre, calificacion_edad)values('Pelicula2',12);
insert into Peliculas (nombre, calificacion_edad)values('Pelicula3',18);
insert into Peliculas (nombre, calificacion_edad)values('Pelicula4',12);
insert into Peliculas (nombre, calificacion_edad)values('Pelicula5',0);

insert into Salas(nombre, pelicula) values('Sala1', '1');
insert into Salas(nombre, pelicula) values('Sala2', '2');
insert into Salas(nombre, pelicula) values('Sala3', '3');
insert into Salas(nombre, pelicula) values('Sala4', '4');
insert into Salas(nombre, pelicula) values('Sala5', '5');
