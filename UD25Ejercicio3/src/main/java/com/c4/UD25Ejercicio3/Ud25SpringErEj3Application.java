package com.c4.UD25Ejercicio3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud25SpringErEj3Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud25SpringErEj3Application.class, args);
	}

}
