package com.c4.UD25Ejercicio3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD25Ejercicio3.dao.IAlmacenesDAO;
import com.c4.UD25Ejercicio3.dto.Almacenes;



@Service
public class AlmacenesServiceImpl implements IAlmacenesService{
	
	@Autowired
	IAlmacenesDAO iAlmacenesDAO;

	@Override
	public List<Almacenes> listarAlmacenes() {
		return iAlmacenesDAO.findAll();
	}

	@Override
	public Almacenes guardarAlmacen(Almacenes Almacenes) {
		return iAlmacenesDAO.save(Almacenes);
	}

	@Override
	public Almacenes almacenXID(Long id) {
		return iAlmacenesDAO.findById(id).get();
	}

	@Override
	public Almacenes actualizarAlmacen(Almacenes Almacenes) {
		return iAlmacenesDAO.save(Almacenes);
	}

	@Override
	public void eliminarAlmacen(Long id) {
		iAlmacenesDAO.deleteById(id);
		
	}

	
	
	

}


