package com.c4.UD25Ejercicio3.service;

import java.util.List;

import com.c4.UD25Ejercicio3.dto.Almacenes;




public interface IAlmacenesService {

	//Metodos del CRUD
	public List<Almacenes> listarAlmacenes(); //Listar All 
	
	public Almacenes guardarAlmacen(Almacenes Almacenes);	//Guarda un Almacen CREATE
	
	public Almacenes almacenXID(Long id); //Leer datos de un Almacen READ
	
	public Almacenes actualizarAlmacen(Almacenes Almacenes); //Actualiza datos del Almacen UPDATE
	
	public void eliminarAlmacen(Long id);// Elimina el Almacen DELETE
	
	
}
