package com.c4.UD25Ejercicio3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD25Ejercicio3.dto.Cajas;
import com.c4.UD25Ejercicio3.service.CajasServiceImpl;


@RestController
@RequestMapping("/api")
public class CajasController {

	@Autowired
	CajasServiceImpl cajasServideImpl;

	@GetMapping("/cajas")
	public List<Cajas> listarCajas() {
		return cajasServideImpl.listarCajas();
	}

	@PostMapping("/cajas")
	public Cajas salvarCaja(@RequestBody Cajas caja) {

		return cajasServideImpl.guardarCaja(caja);
	}

	@GetMapping("/cajas/{id}")
	public Cajas cajaoXID(@PathVariable(name = "id") String id) {

		Cajas caja_xid = new Cajas();
		
		caja_xid = cajasServideImpl.cajaXRef(id);

		System.out.println("articulo XID: " + caja_xid);

		return caja_xid;
	}

	@PutMapping("/cajas/{id}")
	public Cajas actualizarCaja(@PathVariable(name = "id") String id, @RequestBody Cajas caja) {

		Cajas caja_seleccionado = new Cajas();
		Cajas caja_actualizado = new Cajas();

		caja_seleccionado = cajasServideImpl.cajaXRef(id);

		caja_seleccionado.setContenido(caja.getContenido());
		caja_seleccionado.setValor(caja.getValor());
		caja_seleccionado.setNumReferencia(caja.getNumReferencia());
		caja_seleccionado.setAlmacenes(caja.getAlmacenes());

		caja_actualizado = cajasServideImpl.actualizarCaja(caja_seleccionado);

		System.out.println("El articulo actualizado es: " + caja_actualizado);

		return caja_actualizado;
	}

	@DeleteMapping("/cajas/{id}")
	public void eliminarCaja(@PathVariable(name = "id") String id) {
		cajasServideImpl.eliminarCaja(id);;
	}

}
