package com.c4.UD25Ejercicio3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c4.UD25Ejercicio3.dto.Almacenes;
import com.c4.UD25Ejercicio3.service.AlmacenesServiceImpl;


@RestController
@RequestMapping("/api")
public class AlmacenesController {

	@Autowired
	AlmacenesServiceImpl almacenesServideImpl;

	@GetMapping("/almacenes")
	public List<Almacenes> listarFabricante() {
		return almacenesServideImpl.listarAlmacenes();
	}

	@PostMapping("/almacenes")
	public Almacenes salvarFabricante(@RequestBody Almacenes almacen) {

		return almacenesServideImpl.guardarAlmacen(almacen);
	}

	@GetMapping("/almacenes/{id}")
	public Almacenes fabricanteXID(@PathVariable(name = "id") Long id) {

		Almacenes almacen_xid = new Almacenes();

		almacen_xid = almacenesServideImpl.almacenXID(id);

		System.out.println("almacen XID: " + almacen_xid);

		return almacen_xid;
	}

	@PutMapping("/almacenes/{id}")
	public Almacenes actualizarFabricante(@PathVariable(name = "id") Long id, @RequestBody Almacenes almacen) {

		Almacenes almacen_seleccionado = new Almacenes();
		Almacenes almacen_actualizado = new Almacenes();

		almacen_seleccionado = almacenesServideImpl.almacenXID(id);

		almacen_seleccionado.setCapacidad(almacen.getCapacidad());
		almacen_seleccionado.setLugar(almacen.getLugar());
		

		almacen_actualizado = almacenesServideImpl.actualizarAlmacen(almacen_seleccionado);

		System.out.println("El fabricante actualizado es: " + almacen_actualizado);

		return almacen_actualizado;
	}

	@DeleteMapping("/almacenes/{id}")
	public void eliminarFabricante(@PathVariable(name = "id") Long id) {
		almacenesServideImpl.eliminarAlmacen(id);;
	}

}
