DROP table IF EXISTS Cajas;
DROP table IF EXISTS Almacenes;

CREATE TABLE Almacenes (
	id INT AUTO_INCREMENT, 
	lugar NVARCHAR(100), 
	capacidad INT,
	PRIMARY KEY (id)
);

CREATE TABLE Cajas (
	num_Referencia CHAR(5),
	contenido NVARCHAR(100),
	valor INT,
	id_Almacen INT NOT NULL,
	PRIMARY KEY (num_Referencia),
	FOREIGN KEY (id_Almacen) REFERENCES Almacenes(id) ON DELETE cascade ON UPDATE CASCADE
);

insert into Almacenes(lugar, capacidad) values('almacen1', 10);
insert into Almacenes(lugar, capacidad) values('almacen2', 20);
insert into Almacenes(lugar, capacidad) values('almacen3', 5);
insert into Almacenes(lugar, capacidad) values('almacen4', 15);

insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('aa', 'Clips', 200, 3);
insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('bb', 'Papeles', 200, 3);
insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('cc', 'Grapas', 200, 3);
insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('dd', 'Mesas', 50, 1);
insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('ee', 'Sillas', 100, 1);
insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('ff', 'Portatiles', 100, 4);
insert into Cajas(num_Referencia, contenido, valor, id_Almacen) values('gg', 'Ratones', 100, 4);
