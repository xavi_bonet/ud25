DROP table IF EXISTS Empleados;
DROP table IF EXISTS Departamentos;

create table Departamentos(
    codigo int not null,
    nombre nvarchar(100),
    presupuesto int,
    PRIMARY KEY (codigo)
);

create table Empleados(
    dni varchar(8) not null,
    nombre nvarchar(100),
    apellidos nvarchar(100),
    departamento int not null,
    PRIMARY KEY (dni),
    FOREIGN KEY (departamento) REFERENCES Departamentos(codigo)
);

insert into Departamentos (codigo, nombre, presupuesto)values(1,'Dep1',2200);
insert into Departamentos (codigo, nombre, presupuesto)values(2,'Dep2',1200);
insert into Departamentos (codigo, nombre, presupuesto)values(3,'Dep3',5200);
insert into Departamentos (codigo, nombre, presupuesto)values(4,'Dep4',1000);
insert into Departamentos (codigo, nombre, presupuesto)values(5,'Dep5',8300);

insert into Empleados(dni, nombre, apellidos, departamento) values('4793882K', 'Xavi', 'Bonet Daga', 5);
insert into Empleados(dni, nombre, apellidos, departamento) values('1234567A', 'LoremIpsum', 'LoremIpsum', 5);
insert into Empleados(dni, nombre, apellidos, departamento) values('7654321A', 'LoremIpsum', 'LoremIpsum', 5);
insert into Empleados(dni, nombre, apellidos, departamento) values('2134567A', 'LoremIpsum', 'LoremIpsum', 5);
insert into Empleados(dni, nombre, apellidos, departamento) values('3214567A', 'LoremIpsum', 'LoremIpsum', 5);