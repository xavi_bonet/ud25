package com.c4.UD25Ejercicio2.service;

import java.util.List;

import com.c4.UD25Ejercicio2.dto.Departamento;

public interface IDepartamentoService {

	public List<Departamento> listarDepartamento();
	
	public Departamento guardarDepartamento(Departamento departamento);
	
	public Departamento departamentoXID(int codigo);
	
	public Departamento actualizarDepartamento(Departamento departamento);
	
	public void eliminarDepartamento(int codigo);
	
	public List<Departamento> listarDepartamentoNomnbre(String nombre);
}
