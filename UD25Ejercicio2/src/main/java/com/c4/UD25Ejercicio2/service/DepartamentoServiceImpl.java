package com.c4.UD25Ejercicio2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.c4.UD25Ejercicio2.dao.IDepartamentoDAO;
import com.c4.UD25Ejercicio2.dto.Departamento;

@Service
public class DepartamentoServiceImpl implements IDepartamentoService{

	@Autowired
	IDepartamentoDAO iDepartamentoDAO;
	
	@Override
	public List<Departamento> listarDepartamento() {
		return iDepartamentoDAO.findAll();
	}

	@Override
	public Departamento guardarDepartamento(Departamento departamento) {
		return iDepartamentoDAO.save(departamento);
	}

	@Override
	public Departamento departamentoXID(int codigo) {
		return iDepartamentoDAO.findByCodigo(codigo);
	}

	@Override
	public Departamento actualizarDepartamento(Departamento departamento) {
		return iDepartamentoDAO.save(departamento);
	}

	@Override
	public void eliminarDepartamento(int codigo) {
		iDepartamentoDAO.deleteById(codigo);
	}

	@Override
	public List<Departamento> listarDepartamentoNomnbre(String nombre) {
		return iDepartamentoDAO.findByNombre(nombre);
	}


}
