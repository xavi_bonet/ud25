package com.c4.UD25Ejercicio2.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.c4.UD25Ejercicio2.dto.Empleado;

public interface IEmpleadoDAO extends JpaRepository<Empleado, String>{

	public List<Empleado> findByNombre(String nombre);
	
	public Empleado findByDni(String dni);
}
