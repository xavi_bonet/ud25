package com.c4.UD25Ejercicio2.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c4.UD25Ejercicio2.dto.Departamento;

public interface IDepartamentoDAO extends JpaRepository<Departamento, Integer>{

	public List<Departamento> findByNombre(String nombre);
	
	public Departamento findByCodigo(int codigo);
}
