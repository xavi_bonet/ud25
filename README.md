![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)

# UD25 – SPRING REST ER

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| Xavier Bonet Daga | Master | Team Member | 10/03/2021 |   |   |  |
| David Bonet Daga |  | Team Member | 10/03/2021 |   |   |  |

#### 2. Description
```
Ejercicios UD25 – SPRING REST ER
```

#### 3. Link a un demo con el proyecto desplegado: https://gitlab.com/xavi_bonet/ud25.git

```
UD25 – SPRING REST ER / https://gitlab.com/xavi_bonet/ud25.git
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
Apache Maven - http://maven.apache.org/download.cgi
IDE - Spring Tool Suite 4
```
